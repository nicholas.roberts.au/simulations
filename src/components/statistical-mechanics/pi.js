import { html } from '@polymer/lit-element';
import { store } from '../../store.js';
import { connect } from 'pwa-helpers';
import { PageViewElement } from '../page-view-element.js';
import '@granite-elements/granite-c3';
class SimulationPi extends connect(store)(PageViewElement) {
    _render(props) {
        return html`
            <granite-c3 data="${props.data}" axis="${props.axis}"></granite-c3>
            <h1>${props.error}</h1>
            <canvas width="500" height="500">
        `
    }
    static get properties() {
        return {
            data: Object,
            axis: Object,
            error: Number
        }
    }
    estimatePi(n) {
        const ctx = this.shadowRoot.querySelector('canvas').getContext('2d');
        let Obs = 0
        let ObsSquare = 0
        const hits = Array(n).fill(0).map(_ => {
            const [x, y] =  [Math.random() * 2 - 1, Math.random() * 2 - 1];
            const hit = (x ** 2 + y ** 2) < 1;
            ctx.beginPath();
            ctx.strokeStyle = hit ? "red" : "green";
            ctx.arc((x + 1) * 250, (y + 1) * 250, 0.5, 0, 360);
            ctx.stroke();
            ctx.closePath();
            Obs += hit ? 4 : 0;
            ObsSquare += hit ? 16 : 0;
            return hit ? 1 : 0;
        }).reduce((a, b) => a + b);
        return [4.0 * hits / n, Obs, ObsSquare];
    }
    _stateChanged(change) {
    }
    _firstRendered() {
        console.log(this.shadowRoot.querySelector('div'));
        const ctx = this.shadowRoot.querySelector('canvas').getContext('2d');
        ctx.strokeStyle = 'red';
        ctx.beginPath();
        ctx.arc(250, 250, 250, 0, 360);
        ctx.stroke();
        ctx.strokeStyle = 'black';
        let rms = 0;
        let guesses = [];
        let _Obs = 0;
        let _ObsSquare = 0;
        let n_trials = 10000;
        for(const iter in Array(100).fill(0)) {
            const [hits, Obs, ObsSquare] = this.estimatePi(1000);
            _Obs += Obs;
            _ObsSquare += ObsSquare;
            this.error = Math.sqrt((ObsSquare / n_trials) - (Obs / n_trials) ** 2);
            guesses = guesses.concat(hits);
            console.log(guesses[guesses.length - 1]);
        }
        console.log(guesses)
        const reduced_estimate = guesses.reduce(([total, current_guess, iter], curr) => {
            return [total + curr, (total + current_guess) / (iter + 1), iter + 1]
        }, [0, 0, 0])
        console.log(reduced_estimate);
        let estimate = 0;
        let total = 0;
        let errors = guesses.map((v, idx) => {
            total += v;
            estimate = ((idx * estimate) + v) / (idx + 1);
            // console.log(estimate);
            return estimate;
        })
        console.log(errors)
        this.data = {
            "columns": [
                // ["estimate"].concat(estimate),
            ],
            "type": "scatter",
            // "types": {
            //     "error": "line"
            // }
        };
        this.axis = {
        };
        setTimeout(_ => {
            // this.data.columns[0] = ["estimate"].concat(Array(10).fill(0).map(_ => this.estimatePi(1000)));
            // console.log(this.data);
        }, 1000)
    }
}
customElements.define('simulation-pi', SimulationPi);